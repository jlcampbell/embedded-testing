#include "Arduino.h"
#include <stdbool.h>
#include <stdint.h>

uint8_t pinCalled = 250;
uint8_t modeCalled = 250;

void pinMode(uint8_t pin, uint8_t mode) {
  pinCalled = pin;
  modeCalled = mode;
}

bool pinMode_called_with(uint8_t pin, uint8_t mode) {
  if (pin == pinCalled && mode == modeCalled){
      return true;
  }
  return false;
}