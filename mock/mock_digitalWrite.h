#ifndef _MOCK_DIGITALWRITE_H_
#define _MOCK_DIGITALWRITE_H_

#include <stdbool.h>
#include <stdint.h>

bool digital_write_called_with(uint8_t pinNumber, uint8_t highLow);

#endif