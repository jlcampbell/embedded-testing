#ifndef _MOCK_PINMODE_H_
#define _MOCK_PINMODE_H_

#include <stdbool.h>
#include <stdint.h>

bool pinMode_called_with(uint8_t pin, uint8_t mode);

#endif