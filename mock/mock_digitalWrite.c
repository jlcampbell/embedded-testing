#include "Arduino.h"

uint8_t pin = 200;
uint8_t high = 200;

void digitalWrite(uint8_t pinNumber, uint8_t highLow){
    pin = pinNumber;
    high = highLow;
}

bool digital_write_called_with(uint8_t pinNumber, uint8_t highLow){
    if (pin == pinNumber && high == highLow) {
        return true;
    }
    return false;
}