#include <Arduino.h>

#include "blink.h"

void blink_setup(void)
{
	pinMode(LED_BUILTIN, OUTPUT);
	// Do wonderous things
}

void blink_loop(void)
{
	digitalWrite(PINNUMBER, HIGHLOW);
	// Do wonderous things for ever and ever
}
