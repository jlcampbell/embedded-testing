#ifndef _BLINK_H_
#define _BLINK_H_

// The above lines prevent this header from being
// included in compilation more than once in the
// same compile unit.

void blink_setup(void);
void blink_loop(void);

#endif
