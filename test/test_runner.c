#include "unity.h"
#include "unity_fixture.h"

TEST_GROUP_RUNNER(Blink) {
    RUN_TEST_CASE(Blink, Setup_byDefault_SetsPinmodeToOutput);
    RUN_TEST_CASE(Blink, Loop_SetsDigitalWrite);

}

